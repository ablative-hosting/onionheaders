package main

import (
	"github.com/advancedlogic/GoOse"
)

// CoreConf describes the configuration that dictates how the server works
type CoreConf struct {
	Debug       bool   `json:"debug"`
	TLS         bool   `json:"tls_enabled"`
	TLSKey      string `json:"tls_private_key"`
	TLSCert     string `json:"tls_certificate"`
	FQDN        string `json:fqdn"`
	ListenIP    string `json:"listen_ip"`
	ListenIPv6  string `json:"listen_ipv6"`
	ListenPort  int64  `json:"listen_port"`
	SOCKSConfig string `json:"socks_config"`
}

type CheckResponse struct {
	Score         int                 `json:"score"`
	Onion         string              `json:"onion"`
	Proto         string              `json:"proto"`
	Status        string              `json:"status"`
	StatusCode    int                 `json:"status_code"`
	StaticHeaders OHH                 `json:"static_headers"`
	Headers       []OH                `json:"onion_headers"`
	RawHeaders    map[string][]string `json:"raw_headers"`
	Uncompressed  bool                `json:"uncompressed"`
	Body          string
	PageMeta      *goose.Article
}

type OHH struct {
	StrictTransportSecurity OH `json:"hsts"`
	XXssProtection          OH `json:"x-xss"`
	XFrameOptions           OH
	XContentTypeOptions     OH
	ReferrerPolicy          OH
	FeaturePolicy           OH
	ContentSecurityPolicy   OH
}

type OH struct {
	Score    int
	Header   string
	Value    string
	Elements map[string]string
}

type GenericNotification struct {
	ContactEmail string `json:"email"`
	GPGKey       string `json:"gpg"`
	Identifier   string `json:"id"`
	Meta         string `json:"message"`
	Up           bool   `json:"up"`
}

// Error reports
type ErrorOutput struct {
	Error  string
	ErrNum int
}

type GenericMessage struct {
	Message string
}
