package main

import (
	//"database/sql"
	//_ "github.com/go-sql-driver/mysql"
	"net/http"
	"os"
	"strconv"
)

const (
	APIVERSION = 1
)

func main() {
	var conf CoreConf

	//Env Overrides
	conf.ListenIP = os.Getenv("LISTEN_IP")
	conf.ListenPort = 8090
	conf.SOCKSConfig = os.Getenv("SOCKSCONFIG")
	conf.FQDN = os.Getenv("FQDN")

	//Sanity checks
	if conf.ListenIP == "" {
		conf.ListenIP = "0.0.0.0"
	}

	if conf.FQDN == "" {
		conf.FQDN = "onionheaders.website"
	}

	if conf.SOCKSConfig == "" {
		conf.SOCKSConfig = "torsocks:9050"
	}

	//Handle static web pages
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) { WebIndex(w, r) })
	http.HandleFunc("/about/", func(w http.ResponseWriter, r *http.Request) { WebAbout(w, r) })

	//Help stuff
	http.HandleFunc("/help/feature-policy/", func(w http.ResponseWriter, r *http.Request) { WebHelp(w, r, "feature-policy") })
	http.HandleFunc("/help/strict-transport-security/", func(w http.ResponseWriter, r *http.Request) { WebHelp(w, r, "strict-transport-security") })
	http.HandleFunc("/help/x-frame-options/", func(w http.ResponseWriter, r *http.Request) { WebHelp(w, r, "x-frame-options") })
	http.HandleFunc("/help/x-content-type-options/", func(w http.ResponseWriter, r *http.Request) { WebHelp(w, r, "x-content-type-options") })
	http.HandleFunc("/help/x-xss-protection/", func(w http.ResponseWriter, r *http.Request) { WebHelp(w, r, "x-xss-protection") })
	http.HandleFunc("/help/referrer-policy/", func(w http.ResponseWriter, r *http.Request) { WebHelp(w, r, "referrer-policy") })
	http.HandleFunc("/help/content-security-policy/", func(w http.ResponseWriter, r *http.Request) { WebHelp(w, r, "content-security-policy") })
	http.HandleFunc("/help/compression/", func(w http.ResponseWriter, r *http.Request) { WebHelp(w, r, "compression") })

	//Handle functionality
	http.HandleFunc("/check/", func(w http.ResponseWriter, r *http.Request) { WebCheck(w, r, conf) })
	http.HandleFunc("/top/", func(w http.ResponseWriter, r *http.Request) { WebTop(w, r) })
	//http.HandleFunc("/results/", func(w http.ResponseWriter, r *http.Request) { WebResults(w, r, conf) })

	//http.HandleFunc("/v1/check/", func(w http.ResponseWriter, r *http.Request) { APICheck(w, r, conf) })

	ListenPort := strconv.FormatInt(conf.ListenPort, 10)
	if conf.TLS {
		http.ListenAndServeTLS(":"+ListenPort, conf.TLSCert, conf.TLSKey, nil)
	} else {
		http.ListenAndServe(":"+ListenPort, nil)
	}
}
