package main

import (
	"html/template"
	"log"
	"net/http"
)

// WebIndex handles requests to /
func WebIndex(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Strict-Transport-Security", "max-age=3600; includeSubdomains")

	tmpl, err := template.New("index").ParseFiles("assets/templates/index.html")
	err = tmpl.Execute(w, nil)

	if err != nil {
		log.Fatal(err)
	}

}

// WebAbout handles requests to /about/
func WebAbout(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Strict-Transport-Security", "max-age=3600; includeSubdomains")

	tmpl, err := template.New("about").ParseFiles("assets/templates/about.html")

	err = tmpl.Execute(w, nil)

	if err != nil {
		log.Fatal(err)
	}

}

func WebTop(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Strict-Transport-Security", "max-age=3600; includeSubdomains")

	tmpl, err := template.New("top").ParseFiles("assets/templates/top.html")

	err = tmpl.Execute(w, nil)

	if err != nil {
		log.Fatal(err)
	}

}

func WebHelp(w http.ResponseWriter, r *http.Request, topic string) {
	w.Header().Set("Strict-Transport-Security", "max-age=3600; includeSubdomains")

	//tmpl, err := template.New("help").ParseFiles("assets/templates/help/" + topic + ".html")
	tmpl, err := template.New(topic).ParseGlob("assets/templates/help/*.html")

	err = tmpl.Execute(w, nil)

	if err != nil {
		log.Fatal(err)
	}

}
