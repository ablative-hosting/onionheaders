package main

import (
	"errors"
	"fmt"
	"github.com/advancedlogic/GoOse"
	"github.com/btcsuite/go-socks/socks"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"time"
)

func WebCheck(w http.ResponseWriter, r *http.Request, conf CoreConf) {

	if r.Method == "POST" {

		r.ParseForm()

		if r.FormValue("onion") == "" {
			w.WriteHeader(http.StatusNotFound)
			fmt.Fprintf(w, "Onion Field was Blank")
			return
		}

		checkResponse, getErr := GetHeaders(r.FormValue("onion"), conf)

		if getErr != nil {
			log.Println(getErr)
			w.Header().Set("ow-success", "false")
			w.WriteHeader(http.StatusBadRequest)
			tmpl, err := template.New("error").ParseFiles("assets/templates/error.html")
			err = tmpl.Execute(w, GenericMessage{Message: "There was an error processing your request; " + getErr.Error()})

			if err != nil {
				log.Println(err)
			}
			return
		}

		processResponse(&checkResponse)

		tmpl, err := template.New("check_results").ParseFiles("assets/templates/check_results.html")
		err = tmpl.Execute(w, checkResponse)
		if err != nil {
			log.Println(err)
		}
		return

	} else if r.Method == "GET" {
		//We don't accept PUT / DELETE etc on the /create/ URL
		w.Header().Set("ow-success", "false")
		w.WriteHeader(http.StatusBadRequest)
		log.Printf("Method %s is not allowed!", r.Method)
		fmt.Fprintf(w, "Method %s is not allowed!", r.Method)
	}
}

func processResponse(response *CheckResponse) {
	for k, v := range response.RawHeaders {
		switch k {
		case "Feature-Policy":
			response.Score += 10
			response.StaticHeaders.FeaturePolicy.Score = 10
			response.StaticHeaders.FeaturePolicy.Value = fmt.Sprintf("%v", v)
		case "X-Frame-Options":
			response.Score += 20
			response.StaticHeaders.XFrameOptions.Score = 20
			response.StaticHeaders.XFrameOptions.Value = fmt.Sprintf("%v", v)
		case "Strict-Transport-Security":
			response.Score += 5
			response.StaticHeaders.StrictTransportSecurity.Score = 5
			response.StaticHeaders.StrictTransportSecurity.Value = fmt.Sprintf("%v", v)
		case "X-Content-Type-Options":
			response.Score += 5
			response.StaticHeaders.XContentTypeOptions.Score = 5
			response.StaticHeaders.XContentTypeOptions.Value = fmt.Sprintf("%v", v)
		case "X-Xss-Protection":
			response.Score += 5
			response.StaticHeaders.XXssProtection.Score = 5
			response.StaticHeaders.XXssProtection.Value = fmt.Sprintf("%v", v)
		case "Referrer-Policy":
			response.Score += 10
			response.StaticHeaders.ReferrerPolicy.Score = 10
			response.StaticHeaders.ReferrerPolicy.Value = fmt.Sprintf("%v", v)
		case "Content-Security-Policy":
			response.Score += 20
			response.StaticHeaders.ContentSecurityPolicy.Score = 20
			response.StaticHeaders.ContentSecurityPolicy.Value = fmt.Sprintf("%v", v)
		default:
			response.Score += 1
			response.Headers = append(response.Headers, OH{Score: 1, Header: k, Value: fmt.Sprintf("%v", v)})
		}
	}

	if response.Uncompressed {
		response.Score += -20
		response.Headers = append(response.Headers, OH{Score: -20, Header: "Compression", Value: "Response was not compressed"})
	} else {
		response.Score += 10
		response.Headers = append(response.Headers, OH{Score: 10, Header: "Compression", Value: "Response was compressed"})
	}

	g := goose.New()
	article, gooseErr := g.ExtractFromRawHTML(response.Body, response.Onion)
	if gooseErr == nil {
		response.PageMeta = article
	}

}

func GetHeaders(onion string, conf CoreConf) (CheckResponse, error) {
	defaultResponse := CheckResponse{}
	var client http.Client

	proxy := &socks.Proxy{conf.SOCKSConfig, "", "", true}

	tr := &http.Transport{
		Dial: proxy.Dial,
	}
	client = http.Client{
		Transport: tr,
		Timeout:   time.Duration(20 * time.Second),
	}

	url, urlParseErr := url.Parse(onion)

	if urlParseErr != nil {
		return defaultResponse, urlParseErr
	}

	if url.Host == "" {
		return defaultResponse, errors.New("Must specify http:// or https://")
	}

	if url.Scheme != "" {
		switch url.Scheme {
		case "http":
		case "https":
			log.Println("Scheme is HTTP(S)")
		default:
			log.Println("Unsupported scheme")
			return defaultResponse, errors.New("Invalid Scheme")
		}
	} else {
		url.Scheme = "http"
	}

	req, httpReqErr := http.NewRequest("GET", url.Scheme+"://"+url.Host, nil)

	if httpReqErr != nil {
		return defaultResponse, httpReqErr
	}

	req.Header.Set("User-Agent", "OnionHeaders.website/1.0 (+https://onionheaders.website/bot/)")

	resp, httpErr := client.Do(req)
	if httpErr != nil {
		return defaultResponse, httpErr
	}

	defer resp.Body.Close()

	body, _ := ioutil.ReadAll(resp.Body)

	if resp.StatusCode == 200 {
		defaultResponse.Onion = onion
		defaultResponse.RawHeaders = resp.Header
		defaultResponse.Status = resp.Status
		defaultResponse.StatusCode = resp.StatusCode
		defaultResponse.Proto = resp.Proto
		defaultResponse.Uncompressed = resp.Uncompressed
		// Eventually we'll parse any style or script tags to compare against the CSP header
		// but for now we just use it to show the user the title and description meta tags
		defaultResponse.Body = string(body)

		return defaultResponse, nil
	} else {
		return defaultResponse, errors.New(".onion responded with a non 200 response code")
	}

	return defaultResponse, nil
}
