FROM scratch
ADD https://curl.haxx.se/ca/cacert.pem /etc/ssl/certs/
COPY onionheaders-docker /onionheaders
COPY assets /assets
EXPOSE 8090

ENTRYPOINT ["/onionheaders"]
